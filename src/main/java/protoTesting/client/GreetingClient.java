package protoTesting.client;


// import com.protoTest.dummy.DummyServiceGrpc;
import com.protoTest.dummy.DummyServiceGrpc;
import com.protoTest.greet.*;
import io.grpc.*;
import io.grpc.stub.StreamObserver;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class GreetingClient {

    public static void main(String[] args) {
        System.out.println("Hello im client");

        GreetingClient main = new GreetingClient();
        main.run();

    }



    ManagedChannel channel;

    public void run(){
        channel = ManagedChannelBuilder.forAddress("localhost", 50051)
                .usePlaintext()
                .build();
        doUnaryCall(channel);
        doServerStreamingCall(channel);
        doClientStreamingCall(channel);
        doBiDiStreamingCall(channel);
        doUnaryCallWithDeadline(channel);
        // do something
        System.out.println("Shutting down channel");
        channel.shutdown();

    }
    private void doUnaryCall(ManagedChannel channel){

        //DummyServiceGrpc.DummyServiceBlockingStub syncClient = DummyServiceGrpc.newBlockingStub(channel);
        // DummyServiceGrpc.DummyServiceFutureStub asyncClient = DummyServiceGrpc.newFutureStub(channel);

        // skapade en greeting service klient (blocking - synchronus)
        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);

        // Unary
        // skapar ett protocol buffer greeting message
        Greeting greeting = Greeting.newBuilder()
                .setFirstName("Emanuel")
                .setLastName("Rasmusson")
                .build();

        // samma sak för greetRequest
        GreetRequest greetRequest = GreetRequest.newBuilder()
                .setGreeting(greeting)
                .build();

        // kallar rpc och får tillbaka en response (protocol buffers)
        GreetResponse greetResponse = greetClient.greet(greetRequest);

        System.out.println(greetResponse.getResult());

    }

    private void doServerStreamingCall(ManagedChannel channel){
        // skapade en greeting service klient (blocking - synchronus)
        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);

        // Straming
        //we prepare the request
        GreetManyTimesRequest greetManyTimesRequest =
                GreetManyTimesRequest.newBuilder()
                        .setGreeting(Greeting.newBuilder().setFirstName("Emanuel"))
                        .build();

        //we stream the responses (in a blocking manner)
        greetClient.greetManyTimes(greetManyTimesRequest)
                .forEachRemaining(greetManyTimesResponse -> {
                    System.out.println(greetManyTimesResponse.getResult());
                } );

    }

    private void doClientStreamingCall(ManagedChannel channel){
        //creat a client stub
        //GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);

        GreetServiceGrpc.GreetServiceStub asyncClient = GreetServiceGrpc.newStub(channel);

        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<LongGreetRequest> requestObserver = asyncClient.longGreet(new StreamObserver<LongGreetResponse>() {
            @Override
            public void onNext(LongGreetResponse value) {
                // we get a respond from server
                System.out.println("recived a response from the server");
                System.out.println(value.getResult());
            }

            @Override
            public void onError(Throwable throwable) {
                // we get an error from server

                // onNext will be called only once
            }

            @Override
            public void onCompleted() {
                // the server is done sending us data
                //onCompleted will be called right after onNext();
                System.out.println("Server has completed sending us something");
                latch.countDown();

            }
        });

        // streaming message 1
        System.out.println("Message 1");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("Emanuel")
                        .build())
                .build());

        // streaming message 2
        System.out.println("Message 2");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("Morgan")
                        .build())
                .build());

        // streaming message 3
        System.out.println("Message 3");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("Rogge äger")
                        .build())
                .build());

        //we tell the server that client is done sending data
        requestObserver.onCompleted();
        try {
            latch.await(3L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doBiDiStreamingCall(ManagedChannel channel){

        GreetServiceGrpc.GreetServiceStub asyncClient = GreetServiceGrpc.newStub(channel);

        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<GreetEveryoneRequest> requestObserver = asyncClient.greetEveryone(new StreamObserver<GreetEveryoneResponse>() {
            @Override
            public void onNext(GreetEveryoneResponse value) {
                System.out.println("response from server" + value.getResult());
            }

            @Override
            public void onError(Throwable throwable) {
                latch.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server is done sending data");
                latch.countDown();
            }
        });
        Arrays.asList("Emanuel", "Mark","Rogge","Pogge").forEach(
                name -> {
                    System.out.println("Sending:" + name);

                    requestObserver.onNext(GreetEveryoneRequest.newBuilder()
                            .setGreeting(Greeting.newBuilder()
                                    .setFirstName(name))
                            .build());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        );
        requestObserver.onCompleted();

        try {
            latch.await(3,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doUnaryCallWithDeadline(ManagedChannel channel){
        GreetServiceGrpc.GreetServiceBlockingStub blockingStub = GreetServiceGrpc.newBlockingStub(channel);
        try {
            System.out.println("sending a request with deadline 3000ms");
           GreetWithDeadlineResponse response = blockingStub.withDeadline(Deadline.after(3000, TimeUnit.MILLISECONDS)).greetWithDeadLine(GreetWithDeadlineRequest.newBuilder()
                    .setGreeting(Greeting.newBuilder().setFirstName("Emanuel").getDefaultInstanceForType())
                    .build());
            System.out.println(response.getResult());
        } catch (StatusRuntimeException e){
            if(e.getStatus() == Status.DEADLINE_EXCEEDED){
                System.out.println("Deadline has been exceeded, we dont want the response ");

            }else{
                e.printStackTrace();
            }

        }
        try {
            System.out.println("sending a request with deadline 100ms");
            GreetWithDeadlineResponse response = blockingStub.withDeadline(Deadline.after(100, TimeUnit.MILLISECONDS)).greetWithDeadLine(GreetWithDeadlineRequest.newBuilder()
                    .setGreeting(Greeting.newBuilder().setFirstName("Emanuel").getDefaultInstanceForType())
                    .build());
            System.out.println(response.getResult());
        } catch (StatusRuntimeException e){
            if(e.getStatus() == Status.DEADLINE_EXCEEDED){
                System.out.println("Deadline has been exceeded, we dont want the response ");

            }else{
                e.printStackTrace();
            }

        }
    }

}



