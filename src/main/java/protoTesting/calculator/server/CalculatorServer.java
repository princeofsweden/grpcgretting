package protoTesting.calculator.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class CalculatorServer {

    public static void main(String[] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(50052)
                .addService(new CalculatorServiceImpl())
                .build();

        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread( () ->{
            System.out.println("Shoutdown recived" );
            server.shutdown();
            System.out.println("Server succesfully stopped");
        }));
        server.awaitTermination();

    }
}
