package protoTesting.calculator.client;

import com.protoTest.calculator.CalculatorServiceGrpc;
import com.protoTest.calculator.SumRequest;
import com.protoTest.calculator.SumResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class CalculatorClient {

    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50052)
                .usePlaintext()
                .build();

        CalculatorServiceGrpc.CalculatorServiceBlockingStub stub = CalculatorServiceGrpc.newBlockingStub(channel);

        SumRequest sumRequest = SumRequest.newBuilder()
                .setFirstNumber(13)
                .setSecondNumber(34)
                .build();
        SumResponse response = stub.sum(sumRequest);
        System.out.println(sumRequest.getFirstNumber()+ "+" +sumRequest.getSecondNumber() + "=" + response.getSumResult());

        channel.shutdown();
    }
}
