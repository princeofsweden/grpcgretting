package protoTesting.greeting;


import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class Greetingserver {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("hello grpc");

        Server server = ServerBuilder.forPort(50051)
                .addService(new GreetServiceImpl())
                .build();

        server.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () ->{
            System.out.println("Shoutdown recived" );
            server.shutdown();
            System.out.println("Server succesfully stopped");
        }));

        server.awaitTermination();


    }
}