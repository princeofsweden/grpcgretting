package protoTesting.greeting;

import com.protoTest.greet.*;
import io.grpc.Context;
import io.grpc.stub.StreamObserver;

public class GreetServiceImpl extends GreetServiceGrpc.GreetServiceImplBase {

    @Override
    public void greet(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {

        // extraherar fälten vi behöver
        Greeting greeting = request.getGreeting();
        String firstName = greeting.getFirstName();

        // skapar responsen
        String result = "hello" + firstName;
        GreetResponse response = GreetResponse.newBuilder()
                .setResult(result)
                .build();
        // skickar responsen
        responseObserver.onNext(response);

        // avslutar rpc call
        responseObserver.onCompleted();

        // super.greet(request, responseObserver);

    }
    @Override
    public void greetManyTimes(GreetManyTimesRequest request, StreamObserver<GreetManyTimesResponse> responseObserver) {
        String firstName = request.getGreeting().getFirstName();

        try{
            for (int i = 0; i < 10; i++) {
                String result = "Hello" + firstName + ", response number: " + i;

                GreetManyTimesResponse response = GreetManyTimesResponse.newBuilder()
                        .setResult(result)
                        .build();

                responseObserver.onNext(response);
                Thread.sleep(1000L);

            }
        } catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            responseObserver.onCompleted();
        }





    }

    @Override
    public StreamObserver<LongGreetRequest> longGreet(StreamObserver<LongGreetResponse> responseObserver) {
        StreamObserver<LongGreetRequest> requestObserver = new StreamObserver<LongGreetRequest>() {

            String result = "";

            @Override
            public void onNext(LongGreetRequest value) {
                //Clinet sends a message
                result += "Hello " + value.getGreeting().getFirstName() + "!";

            }

            @Override
            public void onError(Throwable throwable) {
                //client sends an error

            }

            @Override
            public void onCompleted() {
                // client is done
                responseObserver.onNext(
                        LongGreetResponse.newBuilder()
                                .setResult(result)
                                .build()
                );

                //this is when we wants to return a response {responseobserver}
                responseObserver.onCompleted();
            }
        };

        return requestObserver;
    }


    @Override
    public StreamObserver<GreetEveryoneRequest> greetEveryone(StreamObserver<GreetEveryoneResponse> responseObserver) {
        StreamObserver<GreetEveryoneRequest> requestObserver = new StreamObserver<GreetEveryoneRequest>() {
            @Override
            public void onNext(GreetEveryoneRequest value) {
                String result = "Hello" + value.getGreeting().getFirstName();
                GreetEveryoneResponse greetEveryoneResponse = GreetEveryoneResponse.newBuilder()
                        .setResult(result)
                        .build();
                responseObserver.onNext(greetEveryoneResponse);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
        return requestObserver;

    }

    @Override
    public void greetWithDeadLine(GreetWithDeadlineRequest request, StreamObserver<GreetWithDeadlineResponse> responseObserver) {

        Context current = Context.current();
        try {
            for (int i = 0; 1 < 3; i++) {

                if (current.isCancelled()) {


                    System.out.println("sleeeeeeeeep forr 100ms");
                    Thread.sleep(100);

                } else {
                    return;
                }
                System.out.println("send response");
                responseObserver.onNext(GreetWithDeadlineResponse.newBuilder()
                        .setResult("hello" + request.getGreeting().getFirstName())
                        .build());


                responseObserver.onCompleted();


            }
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

